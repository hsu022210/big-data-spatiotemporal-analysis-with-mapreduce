package edu.usfca.cs.mr.solarWind;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Mapper: Reads line by line, split them into words. Emit <word, 1> pairs.
 */
public class SolarWindMapper
extends Mapper<LongWritable, Text, Text, Text> {

    @Override
    protected void map(LongWritable key, Text value, Context context)
        throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString());

        int index = 0;
        String geohash = null;
        String wind = null;
        String cloudCover = null;
        Boolean geoInTargets = false;
        Boolean landCover = false;

        int geohashIndex = 1;
        int windIndex = 15;
        int cloudIndex = 16;
        int landCoverIndex = 18;

        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();

            if (index == geohashIndex){
                geohash = token;

                if (geohash.matches("(9r|9x|9z|9q|9w|9y|9m|9t|9v|dp|dr|dn|dq|dj|dh|c2|c8|cb).*")){
                    geoInTargets = true;
                }

            }else if (index == windIndex){
                wind = token;
            }else if (index == cloudIndex){
                cloudCover = token;
            }else if (index == landCoverIndex && Float.valueOf(token).equals(1.0f)){
                landCover = true;
            }
            index++;
        }

        if (geoInTargets && landCover){
            context.write(new Text("solar and wind"), new Text(geohash + "_" + wind + "_" + cloudCover));
        }

    }
}
