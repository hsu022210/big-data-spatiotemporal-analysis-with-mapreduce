package edu.usfca.cs.mr.solarWind;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.*;

/**
 * Reducer: Input to the reducer is the output from the mapper. It receives
 * word, list<count> pairs.  Sums up individual counts per given word. Emits
 * <word, total count> pairs.
 */
public class SolarWindReducer
extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(
            Text key, Iterable<Text> values, Context context)
    throws IOException, InterruptedException {

        Map<String, List> geoCloudMap = new HashMap<>();
        Map<String, List> geoWindMap = new HashMap<>();

        for(Text val : values){
            String [] arrOfVal = val.toString().split("_");
            String geohash = arrOfVal[0];
            String wind = arrOfVal[1];
            String cloudCover = arrOfVal[2];

            List tmpWindList;
            List tmpCloudList;

            if (!geoCloudMap.containsKey(geohash) && !geoWindMap.containsKey(geohash)){
                tmpWindList = new ArrayList();
                tmpCloudList = new ArrayList();
            } else {
                tmpWindList = geoWindMap.get(geohash);
                tmpCloudList = geoCloudMap.get(geohash);
            }

            tmpWindList.add(wind);
            geoWindMap.put(geohash, tmpWindList);
            tmpCloudList.add(cloudCover);
            geoCloudMap.put(geohash, tmpCloudList);
        }

        Map<String, Float> geoWindAvgMap = getAvgMap(geoWindMap);
        Map<String, Float> geoCloudAvgMap = getAvgMap(geoCloudMap);

        geoWindAvgMap = sortByValue(geoWindAvgMap);
        geoCloudAvgMap = sortByValue(geoCloudAvgMap);

        List<String> geoCloudRankList = new ArrayList<>(geoCloudAvgMap.keySet());
        List<String> geoWindRankList = new ArrayList<>(geoWindAvgMap.keySet());
        Collections.reverse(geoWindRankList);


        Map<String, Float> geoWindCloudRankMap = new HashMap<>();

        for (String geo: geoWindRankList) {
            Float avgRank = (geoWindRankList.indexOf(geo) + geoCloudRankList.indexOf(geo))/ (float)2;
            geoWindCloudRankMap.put(geo, avgRank);
        }

        geoWindCloudRankMap = sortByValue(geoWindCloudRankMap);

        List windResultArr = getTopResultList(geoWindAvgMap, geoWindAvgMap.size()-3, geoWindAvgMap.size());
        List cloudResultArr = getTopResultList(geoCloudAvgMap, 0, 3);
        List<String> windCloudResultArr = getTopResultList(geoWindCloudRankMap, 0, 3);

        String resultStr = "\n";
        resultStr += "wind: " + windResultArr.toString() + "\n";
        resultStr += "cloud: " + cloudResultArr.toString() + "\n";
        resultStr += "wind & cloud: ";

        for (String windCloudResult:windCloudResultArr) {
            String [] arr = windCloudResult.toString().split("_");
            String geohash = arr[0];
            Float windAvg = geoWindAvgMap.get(geohash);
            Float cloudAvg = geoCloudAvgMap.get(geohash);

            resultStr += geohash + "_" + Float.toString(windAvg) + "_" + Float.toString(cloudAvg) + ", ";
        }
        resultStr += "\n";
        context.write(key, new Text(resultStr));
    }


    public static List getTopResultList(Map<String, Float> inputMap, Integer lowerLimit, Integer upperLimit) {
        List resultList = new ArrayList();
        int tmpIndex = 0;

        for (Map.Entry<String, Float> entry : inputMap.entrySet()) {
            if (tmpIndex < upperLimit && tmpIndex >= lowerLimit){
                resultList.add(entry.getKey() + "_" + entry.getValue());
            }
            tmpIndex += 1;
        }
        return resultList;
    }


    public static Map getAvgMap(Map<String, List> inputMap) {
        Map<String, Float> outputMap = new HashMap<>();

        for (Map.Entry<String, List> entry : inputMap.entrySet()) {
            Float sum = 0.0f;
            List<String> tmpList = entry.getValue();

            for (String cloudStr:tmpList) {
                sum += Float.valueOf(cloudStr);
            }

            outputMap.put(entry.getKey(), sum/entry.getValue().size());
        }
        return outputMap;
    }

//  Function below is referenced from here https://www.mkyong.com/java/how-to-sort-a-map-in-java/
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> unsortMap) {

        List<Map.Entry<K, V>> list =
                new LinkedList<>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

}
