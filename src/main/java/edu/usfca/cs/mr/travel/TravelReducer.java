package edu.usfca.cs.mr.travel;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.*;

/**
 * Reducer: Input to the reducer is the output from the mapper. It receives
 * word, list<count> pairs.  Sums up individual counts per given word. Emits
 * <word, total count> pairs.
 */
public class TravelReducer
extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(
            Text key, Iterable<Text> values, Context context)
    throws IOException, InterruptedException {

        HashMap<String, Float> tsTempSumMap = new HashMap<>();
        HashMap<String, Integer> tsTempCountMap = new HashMap<>();

        for(Text val : values){
            String [] arrOfVal = val.toString().split("_");
            String timestamp = arrOfVal[0];
            Float temperature = Float.valueOf(arrOfVal[1]);

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(Long.valueOf(timestamp));
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);

            String yearStr = Integer.toString(year);
            String monthStr = Integer.toString(month);
            String yyyy_mm = yearStr + "_" + monthStr;

            if (!tsTempSumMap.containsKey(yyyy_mm)){
                tsTempSumMap.put(yyyy_mm, temperature);
                tsTempCountMap.put(yyyy_mm, 1);
            } else {
                Float tmp = tsTempSumMap.get(yyyy_mm);
                tsTempSumMap.put(yyyy_mm, tmp + temperature);

                int tmpCount = tsTempCountMap.get(yyyy_mm);
                tsTempCountMap.put(yyyy_mm, tmpCount + 1);
            }
        }


        String coldestMonth = null;
        Float coldestTemp = null;

        for (Map.Entry<String, Float> entry : tsTempSumMap.entrySet()) {
            Float avgTemp = entry.getValue()/tsTempCountMap.get(entry.getKey());

            if (coldestTemp == null || avgTemp < coldestTemp){
                coldestTemp = avgTemp;
                coldestMonth = entry.getKey();
            }
        }

        context.write(key, new Text(coldestMonth + "_" + Float.toString(coldestTemp)));
    }

}
