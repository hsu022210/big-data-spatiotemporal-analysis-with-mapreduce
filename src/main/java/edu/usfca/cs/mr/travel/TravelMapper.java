package edu.usfca.cs.mr.travel;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Date;
import java.text.*;
/**
 * Mapper: Reads line by line, split them into words. Emit <word, 1> pairs.
 */
public class TravelMapper
extends Mapper<LongWritable, Text, Text, Text> {

    @Override
    protected void map(LongWritable key, Text value, Context context)
        throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString());

        int index = 0;
        String geohash = null;
        String timestamp = null;
        Float freezingRain = null;
        Float temperature = null;
        Boolean geoInTargets = false;

        int timestampIndex = 0;
        int geohashIndex = 1;
        int freezingRainIndex = 9;
        int temperatureIndex = 40;

        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();

            if (index == geohashIndex){

                geohash = token;
                if (geohash.matches("(9qq|dr5|dhw|c20|9v6).*")){
                    geoInTargets = true;
                }

            } else if (index == timestampIndex){

                timestamp = token;

            } else if (index == freezingRainIndex){

                freezingRain = Float.valueOf(token);

            } else if (index == temperatureIndex){
                temperature = Float.valueOf(token);
            }
            index++;
        }

        if (geoInTargets && freezingRain.equals(0.0f) && temperature<298 && temperature>288){
            geohash = geohash.substring(0, 3);
            context.write(new Text(geohash), new Text(timestamp + "_" + Float.toString(temperature)));
        }
    }
}
