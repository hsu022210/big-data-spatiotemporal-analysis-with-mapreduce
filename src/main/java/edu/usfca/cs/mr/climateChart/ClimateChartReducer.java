package edu.usfca.cs.mr.climateChart;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Reducer: Input to the reducer is the output from the mapper. It receives
 * word, list<count> pairs.  Sums up individual counts per given word. Emits
 * <word, total count> pairs.
 */
public class ClimateChartReducer
extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(
            Text key, Iterable<Text> values, Context context)
    throws IOException, InterruptedException {

        List<Float> tempList = new ArrayList<>();
        List<Float> rainList = new ArrayList<>();

        for(Text val : values){
            String [] arrOfVal = val.toString().split(",");
            String temperature = arrOfVal[0];
            String rain = arrOfVal[1];
            tempList.add(Float.valueOf(temperature));
            rainList.add(Float.valueOf(rain));
        }

        Float highTemp = Collections.max(tempList);
        Float lowTemp = Collections.min(tempList);

        Float tempSum = 0.0f;
        Float rainSum = 0.0f;

        for (Float temp:tempList) {
            tempSum += temp;
        }

        for (Float rain:rainList) {
            rainSum += rain;
        }

        Float tempAvg = tempSum/tempList.size();
        Float rainAvg = rainSum/rainList.size();

        String [] tmpStrArr = key.toString().split("_");
        String monthNum = tmpStrArr[1];

//        <month-num>  <high-temp>  <low-temp>  <avg-precip>  <avg-temp>

        String result = "";
        result += monthNum + " ";
        result += Float.toString(highTemp) + " ";
        result += Float.toString(lowTemp) + " ";
        result += Float.toString(rainAvg) + " ";
        result += Float.toString(tempAvg);

        context.write(key, new Text(result));
    }

}
