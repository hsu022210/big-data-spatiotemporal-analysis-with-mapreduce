package edu.usfca.cs.mr.climateChart;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Calendar;
import java.util.StringTokenizer;

/**
 * Mapper: Reads line by line, split them into words. Emit <word, 1> pairs.
 */
public class ClimateChartMapper
extends Mapper<LongWritable, Text, Text, Text> {

    @Override
    protected void map(LongWritable key, Text value, Context context)
        throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString());

        int index = 0;
        String geohash;
        String timestamp = null;
        String temperature = null;
        String rain = null;
        Boolean geoTarget = false;

        int timestampIndex = 0;
        int geohashIndex = 1;
        int temperatureIndex = 40;
        int rainIndex = 55;

        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();

            if (index == geohashIndex){

                geohash = token;
                if (geohash.matches("(9qq).*")){
                    geoTarget = true;
                }

            }else if (index == timestampIndex){

                timestamp = token;

            } else if (index == temperatureIndex){

                temperature = token;

            }else if (index == rainIndex){
                rain = token;
            }
            index++;
        }

        if (geoTarget){
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(Long.valueOf(timestamp));

            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            String yyyy_mm = Integer.toString(year) + "_" + Integer.toString(month);

            context.write(new Text(yyyy_mm), new Text(temperature + "," + rain));
        }
    }
}
