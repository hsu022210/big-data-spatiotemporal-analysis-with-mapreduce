package edu.usfca.cs.mr.snowDepth;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Reducer: Input to the reducer is the output from the mapper. It receives
 * word, list<count> pairs.  Sums up individual counts per given word. Emits
 * <word, total count> pairs.
 */
public class SnowDepthReducer
extends Reducer<Text, FloatWritable, Text, FloatWritable> {

    @Override
    protected void reduce(
            Text key, Iterable<FloatWritable> values, Context context)
    throws IOException, InterruptedException {
        Float maxDepth = 0.0f;
        Boolean snowDepthZero = false;

        for(FloatWritable val : values){
            Float snowDepth = val.get();
            if (Float.compare(snowDepth, 0.0f) <= 0){
                snowDepthZero = true;
            } else {
                if (Float.compare(snowDepth, maxDepth) > 0){
                    maxDepth = snowDepth;
                }
            }
        }

        if (!snowDepthZero){
            context.write(key, new FloatWritable(maxDepth));
        }
    }

}
