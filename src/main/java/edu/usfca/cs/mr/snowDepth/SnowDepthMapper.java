package edu.usfca.cs.mr.snowDepth;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Mapper: Reads line by line, split them into words. Emit <word, 1> pairs.
 */
public class SnowDepthMapper
extends Mapper<LongWritable, Text, Text, FloatWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context)
        throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString());

        int index = 0;
        String geohash = null;

        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();
            if (index == 1){
                geohash = token;
            } else if (index == 50){
                String snowDepth = token;
                context.write(new Text(geohash), new FloatWritable(Float.valueOf(snowDepth)));
            }
            index++;
        }
    }
}
