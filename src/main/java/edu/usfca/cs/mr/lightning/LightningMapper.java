package edu.usfca.cs.mr.lightning;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Mapper: Reads line by line, split them into words. Emit <word, 1> pairs.
 */
public class LightningMapper
extends Mapper<LongWritable, Text, Text, Text> {

    @Override
    protected void map(LongWritable key, Text value, Context context)
        throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString());

        int index = 0;
        String geohash = null;

        int geohashIndex = 1;
        int lightningIndex = 22;

        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();

            if (index == geohashIndex){
                geohash = token;
                geohash = geohash.substring(0, 4);
            } else if (index == lightningIndex){
                String lightning = token;
                context.write(new Text("lightning"), new Text( geohash + "_" + lightning));
            }
            index++;
        }
    }
}
