package edu.usfca.cs.mr.lightning;

import edu.usfca.cs.mr.solarWind.SolarWindReducer;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * Reducer: Input to the reducer is the output from the mapper. It receives
 * word, list<count> pairs.  Sums up individual counts per given word. Emits
 * <word, total count> pairs.
 */
public class LightningReducer
extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(
            Text key, Iterable<Text> values, Context context)
    throws IOException, InterruptedException {

        Map geoLightningMap = new HashMap<>();

        for(Text val : values){
            String [] arrOfVal = val.toString().split("_");
            String geohash = arrOfVal[0];
//            String lightning = arrOfVal[1];

//            List tmpList;

            int tmp;

            if (!geoLightningMap.containsKey(geohash)){
//                tmpList = new ArrayList();
                tmp = 1;
            } else{
//                tmpList = (List) geoLightningMap.get(geohash);
                tmp = (int) geoLightningMap.get(geohash) + 1;
            }

            geoLightningMap.put(geohash, tmp);
//            tmpList.add(lightning);
//            geoLightningMap.put(geohash, tmpList);
        }

//        geoLightningMap = SolarWindReducer.getAvgMap(geoLightningMap);
        geoLightningMap = SolarWindReducer.sortByValue(geoLightningMap);
        List goeLightningResultArr = SolarWindReducer.getTopResultList(geoLightningMap, geoLightningMap.size()-3, geoLightningMap.size());

        context.write(key, new Text(goeLightningResultArr.toString()));
    }

}
