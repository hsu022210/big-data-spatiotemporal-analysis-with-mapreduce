package edu.usfca.cs.mr.driest;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Mapper: Reads line by line, split them into words. Emit <word, 1> pairs.
 */
public class DriestMapper
extends Mapper<LongWritable, Text, Text, FloatWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context)
        throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString());

        int index = 0;
        String geohash;
        String timestamp = null;
        Boolean geoInBayArea = false;

        int timestampIndex = 0;
        int geohashIndex = 1;
        int humidityIndex = 12;

        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();

            if (index == geohashIndex){

                geohash = token;
                if (geohash.matches("(9qb|9qc|9q8|9q9).*")) {
                    geoInBayArea = true;
                }

            }else if (index == timestampIndex){

                timestamp = token;

            } else if (index == humidityIndex && geoInBayArea){

                Float humidity = Float.valueOf(token);
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(Long.valueOf(timestamp));

                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                String yyyy_mm = Integer.toString(year) + "_" + Integer.toString(month);

                context.write(new Text(yyyy_mm), new FloatWritable(humidity));
            }
            index++;
        }
    }
}
