package edu.usfca.cs.mr.hottest;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Mapper: Reads line by line, split them into words. Emit <word, 1> pairs.
 */
public class HottestMapper
extends Mapper<LongWritable, Text, Text, Text> {

    @Override
    protected void map(LongWritable key, Text value, Context context)
        throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString());

        int index = 0;
        String geohash = null;
        String timestamp = null;

        int timestampIndex = 0;
        int geohashIndex = 1;
        int temperatureIndex = 40;

        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();

            if (index == geohashIndex){
                geohash = token;
            }else if (index == timestampIndex){
                timestamp = token;
            } else if (index == temperatureIndex){
                String temperature = token;
                context.write(new Text("hottest"), new Text(timestamp + "_" + geohash + "_" + temperature));
            }
            index++;
        }
    }
}
