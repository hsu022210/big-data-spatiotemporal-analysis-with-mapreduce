package edu.usfca.cs.mr.hottest;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Reducer: Input to the reducer is the output from the mapper. It receives
 * word, list<count> pairs.  Sums up individual counts per given word. Emits
 * <word, total count> pairs.
 */
public class HottestReducer
extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(
            Text key, Iterable<Text> values, Context context)
    throws IOException, InterruptedException {

        Float hottestTemp = 0.0f;
        String hottestTimestamp = null;
        String hottestGeohash = null;

        for(Text val : values){
            String [] arrOfVal = val.toString().split("_");
            String timestamp = arrOfVal[0];
            String geohash = arrOfVal[1];
            String temperature = arrOfVal[2];

            Float tempFloat = Float.valueOf(temperature);

            if (tempFloat > hottestTemp){
                hottestTemp = tempFloat;
                hottestGeohash = geohash;
                hottestTimestamp = timestamp;
            }
        }

        context.write(key, new Text(hottestTimestamp + "_" + hottestGeohash + "_" + Float.toString(hottestTemp)));
    }

}
