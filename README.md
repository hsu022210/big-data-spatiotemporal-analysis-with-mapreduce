# Project 2 - Spatiotemporal Analysis with MapReduce

This repository includes starter files and a sample directory structure. You are welcome to use it or come up with your own project structure.

Project Specification: https://www.cs.usfca.edu/~mmalensek/courses/cs686/projects/project-2.html

# Deliverables

The project specification defines several questions that you will answer with MapReduce jobs. You should edit this document (README.md) with your answers as you find them, including figures, references, etc. This will also serve as a way of tracking your progress through the milestones.

## Deliverable I

>How many records are in the dataset? (full)

323759744 records

>Are there any Geohashes that have snow depths greater than zero for the entire year? List some of the top Geohashes. (30%)

Geohash_snowDepth: c1gyqex11wpb_2.5792, c1p5fmbjmkrz_2.71088, c41xurr50ypb_3.4208

![alt text](snow_depth.png)

>When and where was the hottest temperature observed in the dataset? Is it an anomaly? (full)

Not anomaly, the temperature seems reasonable to me since we sometime have that degree in Asia or even in Vegas where the climate is more like desert style.

GMT: Saturday, August 22, 2015 6:00:00 PM

Geohash:d5dpds10m55b, Latitude: 21.02789112, Longitude: -87.07604365, (Somewhere in Mexico, near Quintana Roo)

Temperature: 136.833116˚F

![alt text](hottest.png)

>Where are you most likely to be struck by lightning? Use a precision of 4 Geohash characters and provide the top 3 locations. (30%)

Geohash_struckTimes: 9g0q_3319, 9ge2_3335, 9g7w_3337

![alt text](lightning.png)

>What is the driest month in the bay area? This should include a histogram with data from each month. (Note: how did you determine what data points are in the bay area?) (full)

2015 August, humidity precipitation: 23.547659 (cm), Bay Area geohashes: 9qb|9qc|9q8|9q9

![alt text](chart/driest%20concise.png "chart")

>After graduating from USF, you found a startup that aims to provide personalized travel itineraries using big data analysis. Given your own personal preferences, build a plan for a year of travel across 5 locations. Or, in other words: pick 5 regions. What is the best time of year to visit them based on the dataset? (30%)

Note: no freezing rain, 58.73˚F<temperature<76.73˚F, coldest month

9v6, February, 291.7272, 65.43896˚F (Austin)

9qq, January, 289.82864, 62.021552˚F (Vegas)

dhw, February, 292.81735, 67.40123˚F (Miami)

c20, November, 288.5101, 59.64818˚F (Portland)

dr5, November, 289.43082, 61.305476˚F (New York)

![alt text](bayArea.png)

>Your travel startup is so successful that you move on to green energy; here, you want to help power companies plan out the locations of solar and wind farms across North America. Write a MapReduce job that locates the top 3 places for solar and wind farms, as well as a combination of both (solar + wind farm). You will report a total of 9 Geohashes as well as their relevant attributes (for example, cloud cover and wind speeds). (30%)

geo_wind: c81cjen0n82p_21.268297, c81cd59207eb_21.330797, c81ctmd9mpkp_21.580797

![alt text](wind.png)

geo_cloud: 9t3n5qb4c8up_0.0, 9wbckk2z9j5b_0.0, 9y6qfp86g82p_0.0

![alt text](cloud.png)

geo_wind_cloud: 9mq0gyhpregb_14.205799_0.0, 9mq62eqwqrzz_13.955799_0.0, 9t3z3uskgz5b_13.643299_0.0,

![alt text](solar%26wind.png)

>Given a Geohash prefix, create a climate chart for the region. This includes high, low, and average temperatures, as well as monthly average rainfall (precipitation). (full)

![alt text](chart/report.clim-climate.png)

## Deliverable II

Responses go here.
